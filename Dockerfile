# Set up Dockerfile for blitzjs app

# Install dependencies only when needed
FROM node:alpine AS deps
RUN apk --update add --no-cache curl git python3 alpine-sdk \
  bash autoconf libtool automake make g++ libc6-compat
RUN yarn add sodium-native
WORKDIR /app
COPY package.json yarn.lock ./
# RUN yarn config set unsafe-perm true
RUN yarn install

# Rebuild the source code only when needed
FROM node:alpine AS builder
WORKDIR /app
COPY . .
COPY --from=deps /app/node_modules ./node_modules
ENV NODE_OPTIONS --openssl-legacy-provider
ENV DATABASE_URL "postgresql://aiplus:aiplus@@db:5432/aiplus"
RUN npx blitz prisma generate
CMD ["yarn", "build"]
# Production image, copy all the files and run blitz
FROM node:alpine AS runner
WORKDIR /app

ENV NODE_ENV production

RUN addgroup -g 1001 -S nodejs
RUN adduser -S blitzjs -u 1001

# You only need to copy next.config.js if you are NOT using the default configuration
COPY --from=builder /app/blitz.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder --chown=blitzjs:nodejs /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json

USER blitzjs

EXPOSE 3000

CMD ["yarn", "start"]




# WORKDIR /app

# # Copy package.json and package-lock.json to /app
# COPY package*.json .

# # Install dependencies
# RUN npm install

# # Copy blitzjs app to the container
# COPY . .

# # Declare a env variable PORT at 80 and expose it as variable PORT
# ENV PORT 80
# EXPOSE $PORT


# # Build blitzjs app
# RUN npm run build

# # Run blitzjs app
# CMD ["npm", "start"]