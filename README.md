# Setup
### Install submodules
```bash
git submodules init
git submodules update
```
## Env variables
```bash
POSTGRES_DB="aiplus"
POSTGRES_USER="aiplus"
POSTGRES_PASSWORD="!aiplus@"
POSTGRES_HOST="db"
DATABASE_URL="postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:5432/$POSTGRES_DB"
```
### To run client app
Need to set the `DATABASE_URL` as 
```bash
DATABASE_URL="postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:5432/$POSTGRES_DB"
```

Then `yarn install` and `yarn build` in ./client-app
